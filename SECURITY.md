# Security Policy

## Supported Versions

We release patches for security vulnerabilities. Currently, only the most recent release is eligible for receiving such patches.

## Reporting a Vulnerability

Please report (suspected) security vulnerabilities on the [issue tracker] as a [confidential issue] with a description of the issue, the steps you took to create the issue, affected versions, and, if known, mitigations for the issue.

We will attempt to provide a response within three working days of opening your issue (please note, that this project is volunteer run and response times may be longer). If the issue is confirmed as a vulnerability, we will open a Security Advisory and acknowledge your contributions as part of it.

This project follows a [coordinated vulnerability disclosure (CVD)] with a 90-day disclosure policy.

[issue tracker]: https://gitlab.archlinux.org/archlinux/alpm/alpm-types/-/issues/
[confidential issue]: https://docs.gitlab.com/ee/user/project/issues/confidential_issues.html#in-a-new-issue
[coordinated vulnerability disclosure (CVD)]: https://en.wikipedia.org/wiki/Coordinated_vulnerability_disclosure
