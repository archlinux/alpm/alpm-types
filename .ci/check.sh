#!/bin/bash

set -euxo pipefail

cargo fmt -- --check
cargo clippy --all -- -D warnings
cargo deny check
cargo test --all
codespell
reuse lint
