# Contributing

These are the contribution guidelines for alpm-types.

Development takes place on Arch Linux's GitLab at https://gitlab.archlinux.org/archlinux/alpm/alpm-types.

Announcements in regard to this project are propagated on the [arch-projects mailing list].

Discussion around this project takes place in [`#archlinux-projects`] on [Libera Chat].

Please read our distribution-wide [Code of Conduct](https://terms.archlinux.org/docs/code-of-conduct/) before contributing, to understand what actions will and will not be tolerated.

## Writing code

This project is written in [Rust](https://www.rust-lang.org/) and formatted using the default [rustfmt](https://github.com/rust-lang/rustfmt) of the most recent version of Rust.

All contributions are linted using [clippy](https://github.com/rust-lang/rust-clippy) and spell checked using [codespell](https://github.com/codespell-project/codespell).
The dependencies are linted with [deny](https://github.com/EmbarkStudios/cargo-deny).

To aide in development, it is encouraged to use the local `.ci/check.sh` script as [git pre-commit hook](https://man.archlinux.org/man/githooks.5#pre-commit):

```shell
$ ln -f -s ../../.ci/check.sh .git/hooks/pre-commit
```

## Writing commit messages

The commit message style follows [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/).

To aide in development, install [cocogitto](https://github.com/cocogitto/cocogitto) and add its [git-hooks](https://man.archlinux.org/man/githooks.5):

```shell
$ cog install-hook all
```

## Creating releases

Releases are created by the developers of this project using [cocogitto](https://github.com/cocogitto/cocogitto) by running:

```shell
cog bump --auto
```

The above will automatically:

* increase the version based on the conventional commits since the last release
* add an entry to `CHANGELOG.md`
* test whether semver is honored (using [cargo-semver-checks](https://crates.io/crates/cargo-semver-checks)) with the increased version
* create a tag and push the changes to the default branch of the project

A continuous integration pipeline afterwards takes care of building a crate for the release and pushing it to crates.io.

## License

All code contributions fall under the terms of the [Apache-2.0] and [MIT].

All project documentation contributions fall under the terms of the [CC-BY-SA-4.0](https://creativecommons.org/licenses/by-sa/4.0/).

All configuration file contributions fall under the terms of the [CC0 1.0](https://creativecommons.org/publicdomain/zero/1.0/).

License identifiers and copyright statements are checked using [reuse](https://git.fsfe.org/reuse/tool). By using `.ci/check.sh` as git pre-commit hook, it is run automatically.

[arch-projects mailing list]: https://lists.archlinux.org/mailman3/lists/arch-projects.lists.archlinux.org/
[`#archlinux-projects`]: ircs://irc.libera.chat/archlinux-projects
[Libera Chat]: https://libera.chat/
[Apache-2.0]: LICENSES/Apache-2.0.txt
[MIT]: LICENSES/MIT.txt
